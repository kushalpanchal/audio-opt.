#include<stdio.h>
#include<arm_neon.h>
unsigned short int Databuffer1[]={1,2,3,4};
unsigned short int Databuffer2[]={4,5,1,2};
int main()
{
uint8*4_t V;
uint8*4_t P;
uint8*4_t Output;
V=Vld1_u8(Databuffer1);
P=Vld1_u8(Databuffer2);
Output=Vadd_u8(V,P);
Vst1_u8(Databuffer1,Output);
return 0;
}